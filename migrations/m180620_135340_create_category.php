<?php

use yii\db\Migration;

/**
 * Class m180620_135340_create_category
 */
class m180620_135340_create_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'create_by' => $this->integer(),
            'updated_by' => $this->integer()


        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180620_135340_create_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180620_135340_create_category cannot be reverted.\n";

        return false;
    }
    */
}
